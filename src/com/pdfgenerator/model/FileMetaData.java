package com.pdfgenerator.model;

import java.io.Serializable;

public class FileMetaData implements Serializable {

    private String es;

    public FileMetaData() {
    }

    public FileMetaData(String es) {
        this.es = es;
    }

    public String getEs() {
        return es;
    }

    public void setEs(String table) {
        this.es = table;
    }

}
