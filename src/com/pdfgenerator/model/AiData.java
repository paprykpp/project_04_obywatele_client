package com.pdfgenerator.model;

import java.io.Serializable;
public class AiData implements Serializable {

    private String mark;

    public AiData(){
    }

    public AiData(final String mark){

        this.mark = mark;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String toString(){
        return "AiData{" +
                "mark='" + mark + '\'' +
                '}';
    }
}
