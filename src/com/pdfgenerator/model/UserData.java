package com.pdfgenerator.model;

import java.io.Serializable;


public class UserData implements Serializable {

    private Integer X;
    private Integer Y;
    private String mark;

    public UserData() {
    }

    public UserData(final Integer X,
                    final Integer Y,
                    final String mark) {

        this.X = X;
        this.Y = Y;
        this.mark = mark;
    }

    public Integer getX() {
        return X;
    }

    public Integer getY() {
        return Y;
    }

    public String getMark() {
        return mark;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "X='" + X + '\'' +
                ", Y='" + Y + '\'' +
                ", mark='" + mark + '\'' +
                '}';
    }
}
