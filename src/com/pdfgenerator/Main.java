package com.pdfgenerator;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pdfgenerator.model.AiData;
import com.pdfgenerator.model.FileMetaData;
import com.pdfgenerator.model.ResetData;
import com.pdfgenerator.model.UserData;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class Main {

    public static int wynik=0;
    public static int row = 0;

    public static String exampleHttpClientForGetMethod(String str) {

        final HttpClient client = HttpClientBuilder.create().build();
        /*
            Do konstruktora klasy HttpGet podajemy url z nasza usluga ktora zwaraca JSON'a.
            W tym miejscu tworzymy request serwera.
        */
        final HttpGet request = new HttpGet("http://127.0.0.1:8080/tictactoe/result");

        /* Przy pomocy tej biblioteki zmienimy JSON'a na obiekt typu 'FileMetaData'. */
        final Gson gson = new Gson();
        //StringBuffer std = new StringBuffer(str);
        // s;
        try {

            final HttpResponse response = client.execute(request);  // Otrzymujemy odpowiedz od serwera.
            final HttpEntity entity = response.getEntity();

            final String json = EntityUtils.toString(entity);   // Na tym etapie odczytujemy JSON'a, ale jako String.
            // Wyswietlamy zawartosc JSON'a na standardowe wyjscie.
            //System.out.println("Odczytujemy JSON'a z serwera:");
            System.out.println(json);

            /*
                Tutaj odbywa sie przetworzenie (serializacja) JSON'a (String) na List<FileMetaData>
                Prosze przeanalizowac jak wyglada struktura klasy FileMetaData. Struktura klasy dokladnie
                odwozorowywuje strukture JSON'a zwroconego przez serwer dlatego biblioteka jest w stanie
                poradzic sobie z taka konstrukcja
             */

            //final Type type = new TypeToken<ArrayList<FileMetaData>>(){}.getType();
            //final List<FileMetaData> files = gson.fromJson(json, type);

            /*
                Jestesmy w stanie odczytac kod odpowiedzi serwera.
                Kod odpowiedzi zostanie wyswietlony na standardowym wyjsciu.
            */
            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if (response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Brak danych do wyswietlenia!");
            } else if (response.getStatusLine().getStatusCode() == 200) {
                return json;
                // Dzialamy na obiekcie - mamy dostep do danych, ktore zostaly odczytane z JSON'a
                //System.out.println("Tabela:");
                //for(final FileMetaData file: files) {
                //System.out.printf("Plik: %s, B \n", file.getTable());
                //}
            }

        } catch (IOException e) {

            System.out.println("Houston, we have a problem with GET");
            e.printStackTrace();
        }
        return null;
    }

    public static String exampleHttpClientForPostMethod(Integer X, Integer Y, String Mark) {

        final CloseableHttpClient client = HttpClients.createDefault();
        StringBuilder n1 = new StringBuilder();
        StringBuilder n2 = new StringBuilder();
        StringBuilder n3 = new StringBuilder();
        n1.append("http://127.0.0.1:8080/tictactoe/set-field-by-user?X=");
        n1.append(X);
        n2.append(n1.toString() + "&Y=");
        n2.append(Y);
        n3.append(n2.toString() + "&mark=");
        n3.append(Mark);
        final HttpPost httpPost = new HttpPost(n3.toString());

        Gson gson = new Gson();

        // Tworzymy obiekt uzytkownika
        final UserData userData = new UserData(X,Y,Mark);

        // Serializacja obiektu do JSONa
        final String json = gson.toJson(userData);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 200) {

                System.out.println(("Udało się wykonać ruch"));
            }

            client.close();
        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
        return null;
    }

    public static String exampleHttpClientForPostMethodAi(String Mark){

        final CloseableHttpClient client = HttpClients.createDefault();

        StringBuilder name = new StringBuilder();
        name.append("http://127.0.0.1:8080/tictactoe/set-field-by-ai?mark=");
        name.append(Mark);
        final HttpPost httpPost = new HttpPost(name.toString());

        Gson gson = new Gson();

        // Tworzymy obiekt uzytkownika
        final AiData aiData = new AiData(Mark);

        // Serializacja obiektu do JSONa
        final String json = gson.toJson(aiData);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 201) {

                System.out.println(("Przeciwnik wykonał ruch"));
            }

            client.close();
        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
        return null;
    }

    public  static void exampleHttpClientForPostMethodResetGame(){

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/tictactoe/reset-game");

        Gson gson = new Gson();

        // Tworzymy obiekt uzytkownika
        final ResetData resetData = new ResetData();

        // Serializacja obiektu do JSONa
        final String json = gson.toJson(resetData);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 200) {
                wynik=1;
                System.out.println(("Gra zresetowana"));
            }

            client.close();
        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String XDD = "";
        Integer XX = 1;
        Integer YY = 1;

        //SET FIELD BY USER
       //System.out.println("Test metody SET BY USER: \n");
        //exampleHttpClientForPostMethod(XX,YY,XDD);

        //RESULTS
        //System.out.println("Test metody GET: \n");
        //exampleHttpClientForGetMethod(XDD);
        //System.out.println(XDD);

        //AI
        //System.out.println("test metody Ai :\n");
        //exampleHttpClientForPostMethodAi(XDD);

        //RESETGANE
        //System.out.println("test metody RESET :\n");
        //exampleHttpClientForPostMethodResetGame();
    }
}
