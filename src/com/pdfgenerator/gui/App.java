package com.pdfgenerator.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.pdfgenerator.Main;
import com.pdfgenerator.model.AiData;
import com.sun.prism.impl.FactoryResetException;
import jdk.nashorn.internal.scripts.JO;

import static com.pdfgenerator.Main.*;


public class App {
    private JButton buttonReset;
    private JPanel panelMain;
    private JTextField textField1;
    private JButton aiButton;
    private JButton setButton;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField2;
    private JTextArea textArea1;
    private JButton AiButton;




    private App() {

       final String m1 = "X";
       final String m2 = "O";
       final String m3 = "x";
       final String m4 = "o";
       final String s = "";
        buttonReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exampleHttpClientForPostMethodResetGame();
                if(wynik==1) {
                    JOptionPane.showMessageDialog(null, "Gra zresetowana");
                    textArea1.setText(exampleHttpClientForGetMethod(s));
                }
                else
                    JOptionPane.showMessageDialog(null,"Błąd, gra nie została zresetowana");
            }
        });


        aiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = textField1.getText();

                if(input.equals(m1) || input.equals(m2) || input.equals(m3) || input.equals(m4)) {
                    exampleHttpClientForPostMethodAi(input);
                    textArea1.setText(exampleHttpClientForGetMethod(s));
                    JOptionPane.showMessageDialog(null,"Ai zrobiło ruch");
                }
                else if(input.isEmpty())
                    JOptionPane.showMessageDialog(null,"Nie wpisano znaku");
                else
                    JOptionPane.showMessageDialog(null,"Wybrano zły znak! Ai nie wykonało ruchu \n" +
                            "Możliwe znaki: X,x,O,o");
            }
        });
        setButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int z1=0, z2=0, z3=0;
                if(textField1.getText().isEmpty()){
                    JOptionPane.showMessageDialog(null, "Nie podano znaku");
                    z3++;
                }
                else{
                String input = textField1.getText();
                if(textField4.getText().isEmpty()){
                    JOptionPane.showMessageDialog(null,"Nie podano X");
                    z1++;
                }
                else {
                    Integer x = Integer.parseInt(textField4.getText());
                    if (textField3.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Nie podano Y");
                        z2++;
                    } else {
                        Integer y = Integer.parseInt(textField3.getText());
                        if(z1==0 && z2==0 && z3==0) {
                            if ((input.equals(m1) || input.equals(m2) || input.equals(m3) || input.equals(m4)) && (x.toString().equals("0")
                                    || x.toString().equals("1") || x.toString().equals("2")) && (y.toString().equals("0")
                                    || y.toString().equals("1") || y.toString().equals("2"))) {
                                exampleHttpClientForPostMethod(x, y, input);
                                textArea1.setText(exampleHttpClientForGetMethod(s));
                                JOptionPane.showMessageDialog(null, "Udało się wykonać ruch");
                            }
                            else
                                JOptionPane.showMessageDialog(null, "Wprowadziłeś złe dane !");
                        }
                      }
                    }
                }
            }
        });
    }

    public static void main(String [] args){

        JFrame frame = new JFrame("Kółko i krzyżyk");
        frame.setContentPane(new App().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setSize(800,600);
        frame.setLocationRelativeTo(null);
    }
}
